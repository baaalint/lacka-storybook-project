import { TestComponent } from "src/app/test/test.component";

export default {
  title: "Test"
};

export const toStorybook = () => ({
  component: TestComponent,
  props: {}
});

export const test = () => ({
  component: TestComponent,
  props: {
    text: "123231324"
  }
});

export const nextTest = () => ({
  component: TestComponent,
  props: {
    text: "Jozsi"
  }
});

toStorybook.story = {
  name: "TEST"
};
